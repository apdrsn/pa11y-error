const pa11y = require("pa11y");

pa11y("https://boring-kalam-dd8102.netlify.app", {
  screenCapture: `example.png`,
  chromeLaunchConfig: { ignoreHTTPSErrors: false },
  log: {
    debug: (s) => console.log("logs", s),
    error: (m) => console.error("errors", m),
    info: (t) => console.info("infos", t),
  },
}).then((results) => {
  console.log(results);
});
